// para agregar una funcion de unicidad a los arreglos
Array.prototype.unique=function(a){
    return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
  });

vm_main = new Vue({
    el: '#main',
    created: function () {
        this.functionSemantic()
        this.startYearsMonth()
        this.getUsers()
    },
    data: function () {
        return {
            Years: [],
            Month: [],
            UsersOriginal: [],
            UsersSelect: [],
            Errors: [],
            Relatorios: [],
            Bars: [],
            Graf: null,
            totalMedioFijo: 0,
            totalRecetaire: 0,
            Params: {
                yearFrom: '',
                yearTo: '',
                monthFrom: '',
                monthTo: '',
                consultors: []
            }
        }
    },
    computed: {

    },
    methods: {
        /**
         * Permite Inicializar las funciones de semantics
         */
        functionSemantic: function () {
            $(document).ready(function () {
                // inicializa los dropdown
                $('.ui.dropdown').dropdown({
                    on: 'hover'
                })

                // inicializa los tab
                $('.menu .item').tab();
            })
        },

        /**
         * Permite inicializar los arreglos de año y mes
         */
        startYearsMonth: function () {
            for (let i = 2000; i < 2021; i++) {
                this.Years.push(i);
            }

            this.Month = [
                'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
                'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'
            ]
        },

        /**
         * Permite Obtener a los usuarios
         */
        getUsers: function () {
            let url = route('api.users')
            axios.get(url).then(response => {
                this.UsersOriginal = response.data;
            })
        },

        /**
         * Permite pasar un usuario del un arreglo a otro y eliminarlo del anterior
         * 
         * @param {array} user - informacion del usuario
         * @param {integer} index - indice del arreglo eliminar
         * @param {integer} arreglo - 1 para borar del arreglo original, 2 para borrar del arreglo de selecion
         */
        addUserSelect: function (user, index, arreglo) {
            if (arreglo == 1) {
                this.UsersSelect.push(user)
                this.UsersOriginal.splice(index, 1)
            } else {
                this.UsersOriginal.push(user)
                this.UsersSelect.splice(index, 1)
            }
        },
        /**
         * Permite obtener los relatario de los usuario
         */
        getRelatotios: function () {
            this.Errors = []
            this.Params.consultors = this.UsersSelect
            let url = route('api.relatorio')
            axios.post(url, this.Params).then(response => {
                this.Relatorios = response.data
                $('.large.modal.rela').modal('show')
            }).catch(error => {
                this.Errors = error.response.data.errors
            })
        },

        /**
         * Permite obtener la informacion para los graficos en barra
         */
        getDataBars: function () {
            this.Errors = []
            this.Params.consultors = this.UsersSelect
            let url = route('api.graf')
            axios.post(url, this.Params).then(response => {
                var ctx = document.getElementById('myChart')
                var contexto = ctx.getContext("2d");
                contexto.clearRect(0, 0, ctx.width, ctx.height);
                this.Bars = response.data.datarelatorio
                this.totalMedioFijo = response.data.totalcostomedio
                this.tipoGrafica = 1
                this.createBarLine()
                $('.large.modal.graf').modal('show')
            }).catch(error => {
                this.Errors = error.response.data.errors
            })
        },

        /**
         * Permite tener los datos para la grafica en pizza
         */
        getDataPizza: function () {
            this.Errors = []
            this.Params.consultors = this.UsersSelect
            let url = route('api.graf')
            axios.post(url, this.Params).then(response => {
                var ctx = document.getElementById('myChart2')
                var contexto = ctx.getContext("2d");
                contexto.clearRect(0, 0, 0, 0);
                this.Bars = response.data.datarelatorio
                this.totalRecetaire = response.data.totalRecetaire
                this.createPizza()
                $('.large.modal.graf2').modal('show')
            }).catch(error => {
                this.Errors = error.response.data.errors
            })
        },

        /**
         * Permite obtener colores de forma aleatoria
         */
        getColorRandom: function () {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        },

        /**
         * Permite crear la grafica en barra y el costo medio fijo
         */
        createBarLine: function () {
            let months = []
            let datasets = []
            let datacostoTotal = [] // obtiene la informacion del costo fijo
            let dataSetColores = [] // obtiene los colores para el costoFijo
            let cont = 2;
            let colorCosto = this.getColorRandom()
            this.Bars.forEach(data => {
                let datarece = [] // obtiene la informacion de la recetaide liquidas
                let dataColors = []
                let colorSeT = this.getColorRandom()
                // agrego la información para los dataset de los usuarios
                data.relatorios.forEach(relato => {
                    datarece.push(relato.Receita_liquidad)
                    months.push(relato.month)
                    dataColors.push(colorSeT)
                    
                });
                datasets.push({
                    label: data.usuario,
                    data: datarece,
                    // this dataset is drawn below
                    backgroundColor: dataColors,
                    order: cont
                })
                cont++
            });
            
            months = months.unique()
            // para poder colocar el costo medio fijo en todos los meses
            months.forEach(month => {
                datacostoTotal.push(this.totalMedioFijo)
                dataSetColores.push(colorCosto)
            })
            datasets.push({
                label: 'Costo Fixo Medio',
                data: datacostoTotal,
                type: 'line',
                borderColor: dataSetColores,
                backgroundColor: dataSetColores,
                borderWidth: 2,
                showLine: true,
                fill: false,
                // this dataset is drawn below
                order: 1
            })

            var ctx = document.getElementById('myChart')
            var contexto = ctx.getContext("2d");
            contexto.clearRect(0, 0, 0, 0);

            if (this.Graf != null) {
                this.Graf.destroy()
            }
            this.Graf = new Chart(ctx, {
                type: 'bar',
                data: {
                    datasets: datasets,
                    labels: months
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return 'R$' + value;
                                }
                            }
                        }]
                    }
                }
            })
        },

        /**
         * Permite crear la grafica en forma de pizza
         * 
         */
        createPizza: function () {
            let datasets = []
            let names = []
            let arrayColor = []
            let arrayRecetaire = []
            this.Bars.forEach(data => {
                names.push(data.usuario)
                arrayColor.push(this.getColorRandom())
                arrayRecetaire.push(data.totalRecetaire)
            })
            datasets.push({
                data: arrayRecetaire,
                backgroundColor: arrayColor,
            })
            
            var ctx = document.getElementById('myChart2')
            var contexto = ctx.getContext("2d");
            if (this.Graf != null) {
                this.Graf.destroy()
            }
            this.Graf = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    datasets: datasets,
                    labels: names
                },
                options: {
                    cutoutPercentage: 0,
                }
            })
        }
    }
})
