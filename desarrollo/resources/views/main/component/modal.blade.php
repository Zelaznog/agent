<div class="ui large modal rela">
    <i class="close icon"></i>
    <div class="header">
        Relatorios
    </div>
    <div class="scrolling content">
        <div class="ui segment" v-for="item in Relatorios">
            <h4 class="ui header" v-text="item.usuario"></h4>
            <table class="ui selectable celled table">
                <thead>
                    <tr class="center aligned">
                        <th>Periodo</th>
                        <th>Receita Líquida</th>
                        <th>Custo Fixo</th>
                        <th>Comissão</th>
                        <th>Lucro</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="center aligned" v-for="relatorio in item.relatorios">
                        <td v-text="relatorio.month+' '+ relatorio.year"></td>
                        <td v-text="'R$ '+relatorio.Receita_liquidad" ></td>
                        <td v-text="'R$ '+relatorio.Costo_fijo"></td>
                        <td v-text="'R$ '+relatorio.Comision_sumada"></td>
                        <td v-text="'R$ '+relatorio.Lucro_comision_sumada"></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr class="center aligned">
                        <th>Saldo</th>
                        <th v-text="'R$ '+item.totales.Receita_liquidad"></th>
                        <th v-text="'R$ '+item.totales.Costo_fijo"></th>
                        <th v-text="'R$ '+item.totales.Comision_sumada"></th>
                        <th v-text="'R$ '+item.totales.Lucro_comision_sumada"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    {{-- <div class="actions">
        <div class="ui button">Cancel</div>
        <div class="ui button">OK</div>
    </div> --}}
</div>
