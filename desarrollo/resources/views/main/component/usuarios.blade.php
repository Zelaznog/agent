{{-- botones --}}
<div class="ui three top attached buttons">
    <div class="ui blue basic button" v-on:click="getRelatotios()">
        <img src="{{asset('img/icone_relatorio.png')}}" alt="" class="img_alt">
        Relatório
    </div>
    <div class="ui grey basic button" v-on:click="getDataBars()">
        <img src="{{asset('img/icone_grafico.png')}}" alt="" class="img_alt">
        Grafíco
    </div>
    <button class="ui blue basic button" v-on:click="getDataPizza()">
        <img src="{{asset('img/icone_pizza.png')}}" alt="" class="img_alt">
        Pizza
    </button>
</div>
{{-- cuerpo --}}
<div class="ui attached segment">
    <h4 class="ui header">Consultores</h4>
    <div class="ui stackable two column grid">
        <div class="column">
            <div class="ui segment">
                <div class="ui middle aligned selection animated list h-300" v-if="UsersOriginal.length != 0">
                    <div class="item" v-for="(item, i) in UsersOriginal" v-on:click="addUserSelect(item, i, 1)">
                        <div class="right floated content">
                            <div class="ui positive icon basic button">
                                <i class="plus square icon"></i>
                            </div>
                        </div>
                        <div class="content">
                            <div class="header" v-text="item.usuario"></div>
                        </div>
                    </div>
                </div>
                <div class="ui floating info message" v-else>
                    <p>Sin Usuario Para Selecionar</p>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="ui segment">
                <div class="ui middle aligned selection animated list h-300" v-if="UsersSelect.length != 0">
                    <div class="item" v-for="(item, i) in UsersSelect" v-on:click="addUserSelect(item, i, 2)">
                        <div class="right floated content">
                            <div class="ui negative icon basic button">
                                <i class="close icon"></i>
                            </div>
                        </div>
                        <div class="content">
                            <div class="header" v-text="item.usuario"></div>
                        </div>
                    </div>
                </div>
                <div class="ui floating info message" v-else>
                    <p>Sin Usuario Selecionado</p>
                    {{-- mesaje error --}}
                    <p v-for="error in Errors['consultors']" v-text="error" class="text-red">
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
