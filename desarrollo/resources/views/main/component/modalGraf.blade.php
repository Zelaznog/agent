<div class="ui large modal graf ">
    <i class="close icon"></i>
    <div class="header">
        Grafica Barra
    </div>
    <div class="scrolling content">
        <div class="ui segment" id="canvas">
            <canvas id="myChart" width="400" height="400"></canvas>
        </div>
    </div>
</div>

<div class="ui large modal graf2">
    <i class="close icon"></i>
    <div class="header">
        Grafica Pizza
    </div>
    <div class="scrolling content">
        <div class="ui segment" >
            <canvas id="myChart2" width="400" height="400"></canvas>
        </div>
    </div>
</div>