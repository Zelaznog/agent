<div class="ui segment">
    <h4 class="ui header">Pediodo</h4>
    <div class="ui form">
        <div class="fields">
            <span>DE</span>
            {{-- desde --}}
            <div class="field">
                <label for="">Año</label>
                <select class="ui fluid search dropdown" name="" required v-model.number="Params.yearFrom">
                    <option value="" disabled selected>Selecione Un Año</option>
                    <option :value="item" v-for="item in Years" v-text="item"></option>
                </select>
                <p>
                    <small v-for="error in Errors['yearFrom']" v-text="error" class="text-red">
                    </small>
                </p>
            </div>
            <div class="field">
                <label for="">Mes</label>
                <select class="ui fluid search dropdown" name="" required v-model.number="Params.monthFrom">
                    <option value="" disabled selected>Selecione Un Mes</option>
                    <option :value="(i+1)" v-for="(item, i) in Month" v-text="item"></option>
                </select>
                {{-- mesaje error --}}
                <p>
                    <small v-for="error in Errors['monthFrom']" v-text="error" class="text-red">
                    </small>
                </p>
            </div>

            <span> A </span>
            {{-- hasta --}}
            <div class="field">
                <label for="">Año</label>
                <select class="ui fluid search dropdown" name="" required v-model.number="Params.yearTo">
                    <option value="" disabled selected>Selecione Un Año</option>
                    <option :value="item" v-for="item in Years" v-text="item"></option>
                </select>
                {{-- mesaje error --}}
                <p>
                    <small v-for="error in Errors['yearTo']" v-text="error" class="text-red">
                    </small>
                </p>
            </div>
            <div class="field">
                <label for="">Mes</label>
                <select class="ui fluid search dropdown" name="" required v-model.number="Params.monthTo">
                    <option value="" disabled selected>Selecione Un Mes</option>
                    <option :value="(i+1)" v-for="(item, i) in Month" v-text="item"></option>
                </select>
                {{-- mesaje error --}}
                <p>
                    <small v-for="error in Errors['monthTo']" v-text="error" class="text-red">
                    </small>
                </p>
            </div>
        </div>
    </div>
</div>
