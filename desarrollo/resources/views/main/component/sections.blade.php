<div class="ui top attached tabular menu">
    <a class="item active" data-tab="first">Por Consultor</a>
    <a class="item" data-tab="second">Por Cliente</a>
</div>
<div class="ui bottom attached tab segment active" data-tab="first">
    {{-- fecha y mes --}}
    @include('main.component.desdeHasta')
    {{-- seccion de usuarios --}}
    @include('main.component.usuarios')
    {{-- modal de los relatorios --}}
    @include('main.component.modal')
    {{-- modal de las graficas --}}
    @include('main.component.modalGraf')
</div>
<div class="ui bottom attached tab segment" data-tab="second">
</div>
