<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agent</title>
    {{-- Css Semantic --}}
    <link rel="stylesheet" href="{{asset('Semantic/semantic.min.css')}}">
    {{-- Css Custom --}}
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>
    {{-- header --}}
    @include('layouts.component.header')
    {{-- cuerpo --}}
    <div class="ui container" id="main">
        @yield('content')
    </div>
</body>
{{-- Js Vue --}}
<script src="{{asset('js/component/vue.js')}}"></script>
{{-- Js Axios --}}
<script src="{{asset('js/component/axios.min.js')}}"></script>
{{-- Js Jquery --}}
<script src="{{asset('js/component/jquery.js')}}"></script>
{{-- Js Semantic --}}
<script src="{{asset('Semantic/semantic.min.js')}}"></script>
{{-- Js --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
{{-- Js Custom --}}
@routes
<script src="{{asset('js/custom.js')}}"></script>
</html>