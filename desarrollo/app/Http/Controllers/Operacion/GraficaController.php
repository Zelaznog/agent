<?php

namespace App\Http\Controllers\Operacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GraficaController extends Controller
{
    /**
     * Variable para el filtro de año desde
     *
     * @var integer
     */
    protected $yearFrom = 0;
    /**
     * Variable para el filtro de año hasta
     *
     * @var integer
     */
    protected $yearTo = 0;
    /**
     * Variable para el filtro de mes desde
     *
     * @var integer
     */
    protected $monthFrom = 0;
    /**
     * Variable para el filtro de mes hasta
     *
     * @var integer
     */
    protected $monthTo = 0;
     /**
      * Permite obtener la informacion para la grafica
      *
      * @param Request $data
      * @return void
      */
    public function getGrafiBarra(Request $data)
    {
        $validate = $data->validate([
            'yearFrom' => 'required|numeric|lte:yearTo',
            'yearTo' => 'required|numeric',
            'monthFrom' => 'required|numeric|lte:monthTo',
            'monthTo' => 'required|numeric',
            'consultors' => 'required'
        ]);

        if ($validate) {
            $this->yearFrom = $data->yearFrom;
            $this->yearTo = $data->yearTo;
            $this->monthFrom = $data->monthFrom;
            $this->monthTo = $data->monthTo;
            $select = $this->getStringDataRelatorio();
            $totalCostoFijo = 0;
            $totalRecetaire = 0;
            foreach ($data->consultors as $consultor ) {
                $co_usuario = $consultor['id'];
                $relatorios = DB::table('cao_fatura as f')
                            ->join('cao_os as os', 'f.co_os', '=', 'os.co_os')
                            ->join('cao_salario as sa', 'os.co_usuario', '=', 'sa.co_usuario')
                            ->select(DB::raw($select))
                            ->where('os.co_usuario', '=', $co_usuario)
                            ->where(function ($subwherew){
                                $subwherew->whereYear('f.data_emissao', '>=', $this->yearFrom)
                                            ->whereYear('f.data_emissao', '<=', $this->yearTo);
                            })
                            ->where(function ($subwherew){
                                $subwherew->whereMonth('f.data_emissao', '>=', $this->monthFrom)
                                            ->whereMonth('f.data_emissao', '<=', $this->monthTo);
                            })
                            ->groupBy(DB::raw('YEAR(f.data_emissao)'), DB::raw('MONTH(f.data_emissao)'))
                            ->get();

                $totalRecetaireConsultor = 0;
                foreach ($relatorios as $relato) {
                    $totalCostoFijo = ($totalCostoFijo + $relato->Costo_fijo);
                    $totalRecetaire = ($totalRecetaire + $relato->Receita_liquidad);
                    $totalRecetaireConsultor = ($totalRecetaireConsultor + $relato->Receita_liquidad);
                }
                $arrayUserRelatorio [] = [
                    'usuario' => $consultor['usuario'],
                    'relatorios' => $relatorios,
                    'totalRecetaire' => $totalRecetaireConsultor
                ];
            }
            $totalConsultores = count($data->consultors);
            $data = [
                'datarelatorio' => $arrayUserRelatorio,
                'totalcostomedio' => ($totalCostoFijo/$totalConsultores),
                'totalRecetaire' => $totalRecetaire
            ];
            

            return json_encode($data);
        }
    }

    /**
     * Permite obtener la cadena para tener los datos del relatorio
     *
     * @return string
     */
    private function getStringDataRelatorio()
    {
        return "
            MONTHNAME(f.data_emissao) AS month, 
            YEAR(f.data_emissao) as year,
            abs(SUM(f.valor) - (f.valor * (f.total_imp_inc /100))) as 'Receita_liquidad',
            sa.brut_salario as 'Costo_fijo'
        ";
    }
}
