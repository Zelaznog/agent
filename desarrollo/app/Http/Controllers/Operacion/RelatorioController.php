<?php

namespace App\Http\Controllers\Operacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RelatorioController extends Controller
{

    /**
     * Variable para el filtro de año desde
     *
     * @var integer
     */
    protected $yearFrom = 0;
    /**
     * Variable para el filtro de año hasta
     *
     * @var integer
     */
    protected $yearTo = 0;
    /**
     * Variable para el filtro de mes desde
     *
     * @var integer
     */
    protected $monthFrom = 0;
    /**
     * Variable para el filtro de mes hasta
     *
     * @var integer
     */
    protected $monthTo = 0;

    public function getRelatorio(Request $data)
    {
        $validate = $data->validate([
            'yearFrom' => 'required|numeric|lte:yearTo',
            'yearTo' => 'required|numeric',
            'monthFrom' => 'required|numeric|lte:monthTo',
            'monthTo' => 'required|numeric',
            'consultors' => 'required'
        ]);
        
        if ($validate) {
            $this->yearFrom = $data->yearFrom;
            $this->yearTo = $data->yearTo;
            $this->monthFrom = $data->monthFrom;
            $this->monthTo = $data->monthTo;
            $select = $this->getStringDataRelatorio();
            $arrayUserRelatorio = [];
            foreach ($data->consultors as $consultor ) {
                $totales = [
                    'Receita_liquidad' => 0,
                    'Costo_fijo' => 0,
                    'Comision_sumada' => 0,
                    'Lucro_comision_sumada' => 0,
                ];
                $co_usuario = $consultor['id'];
                $relatorios = DB::table('cao_fatura as f')
                            ->join('cao_os as os', 'f.co_os', '=', 'os.co_os')
                            ->join('cao_salario as sa', 'os.co_usuario', '=', 'sa.co_usuario')
                            ->select(DB::raw($select))
                            ->where('os.co_usuario', '=', $co_usuario)
                            ->where(function ($subwherew){
                                $subwherew->whereYear('f.data_emissao', '>=', $this->yearFrom)
                                            ->whereYear('f.data_emissao', '<=', $this->yearTo);
                            })
                            ->where(function ($subwherew){
                                $subwherew->whereMonth('f.data_emissao', '>=', $this->monthFrom)
                                            ->whereMonth('f.data_emissao', '<=', $this->monthTo);
                            })
                            ->groupBy(DB::raw('YEAR(f.data_emissao)'), DB::raw('MONTH(f.data_emissao)'))
                            ->get();
                
                foreach ($relatorios as $relato) {
                    $totales = [
                        'Receita_liquidad' => ($totales['Receita_liquidad'] + $relato->Receita_liquidad),
                        'Costo_fijo' => ($totales['Costo_fijo'] + $relato->Costo_fijo),
                        'Comision_sumada' => ($totales['Comision_sumada'] + $relato->Comision_sumada),
                        'Lucro_comision_sumada' => ($totales['Lucro_comision_sumada'] + $relato->Lucro_comision_sumada)
                    ];
                    $relato->Receita_liquidad = number_format($relato->Receita_liquidad, 2, ',', '.');
                    $relato->Costo_fijo = number_format($relato->Costo_fijo, 2, ',', '.');
                    $relato->Comision_sumada = number_format($relato->Comision_sumada, 2, ',', '.');
                    $relato->Lucro_comision_sumada = number_format($relato->Lucro_comision_sumada, 2, ',', '.');
                }

                $totales['Receita_liquidad'] = number_format($totales['Receita_liquidad'], 2, ',', '.');
                $totales['Costo_fijo'] = number_format($totales['Costo_fijo'], 2, ',', '.');
                $totales['Comision_sumada'] = number_format($totales['Comision_sumada'], 2, ',', '.');
                $totales['Lucro_comision_sumada'] = number_format($totales['Lucro_comision_sumada'], 2, ',', '.');

                $arrayUserRelatorio [] = [
                    'usuario' => $consultor['usuario'],
                    'relatorios' => $relatorios,
                    'totales' => $totales
                ];
            }

            return json_encode($arrayUserRelatorio);
        }
        
    }

    /**
     * Permite obtener la cadena para tener los datos del relatorio
     *
     * @return string
     */
    private function getStringDataRelatorio()
    {
        return "
        SUM(f.valor) as tvalor, 
        SUM(f.total_imp_inc) as ttotal_imp_inc, 
        MONTHNAME(f.data_emissao) AS month, 
        YEAR(f.data_emissao) as year,
        (SUM(f.valor) * (SUM(f.total_imp_inc) /100)) as 'impuesto previo',
        (SUM(f.valor) - (f.valor * (f.total_imp_inc /100))) as 'Receita_liquidad',
        sa.brut_salario as 'Costo_fijo',
        ((SUM(f.valor) - (f.valor * (f.total_imp_inc /100))) * (f.comissao_cn / 100)) as 'Comision_sumada',
        ((SUM(f.valor) - (f.valor * (f.total_imp_inc /100))) * (f.comissao_cn / 100)) as 'Comision_no_sumada',
        ((SUM(f.valor) - (f.valor * (f.total_imp_inc /100))) - (sa.brut_salario + ((SUM(f.valor) - (f.valor * (f.total_imp_inc /100))) * (f.comissao_cn / 100)))) as 'Lucro_comision_sumada',
        ((SUM(f.valor) - (f.valor * (f.total_imp_inc /100))) - (sa.brut_salario + ((SUM(f.valor) - (f.valor * (f.total_imp_inc /100))) * (f.comissao_cn / 100)))) as 'Lucro_comision_no_sumada'
        ";
    }
}
