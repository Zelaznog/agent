<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class MainController extends Controller
{
    /**
     * Lleva a la vista principal
     *
     * @return void
     */
    public function Index()
    {
        return view('main.index');
    }

    /**
     * Permite tener el Listado de Usuarios
     *
     * @return void
     */
    public function getUsers()
    {
        $users = DB::table('cao_usuario as us')
                ->join('permissao_sistema as ps', 'us.co_usuario', '=', 'ps.co_usuario')
                ->where([
                    ['ps.co_sistema', '=', '1'],
                    ['ps.in_ativo', '=', 's'],
                    ['ps.co_tipo_usuario', '<=', 2]
                ])
                ->select('us.co_usuario as id', 'us.no_usuario as usuario')->get();

        return $users;
    }
}
