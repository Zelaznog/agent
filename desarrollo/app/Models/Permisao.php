<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permisao extends Model
{
    /**
     * Table a la que esta asociada este modelo
     *
     * @var string
     */
    protected $table = 'permissao_sistema';

    /**
     * Llave primaria de este modelo
     *
     * @var string
     */
    protected $primaryKey = 'co_usuario';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'co_tipo_usuario', 'co_sistema', 'in_ativo', 'co_usuario_atualizacao', 'dt_atualizacao'
    ];

    /**
     * Permite obtener la informacion de los usuarios desde este modelo
     *  por medio de la llave primaria co_usuario
     *
     * @return void
     */
    public function Usuarios()
    {
        return $this->belongsTo('App\Post', 'co_usuario', 'co_usuario');
    }
}
