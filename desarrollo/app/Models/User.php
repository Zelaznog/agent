<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Tabla a la que esta asociada este modelo
     *
     * @var string
     */
    protected $table = 'cao_usuario';

    /**
     * Llave primara de la tabla
     *
     * @var string
     */
    protected $primaryKey = 'co_usuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'no_usuario', 'ds_senha', 'co_usuario_autorizacao', 'nu_matricula', 'dt_nascimento', 
        'dt_admissao_empresa', 'dt_desligamento', 'dt_inclusao', 'dt_expiracao', 'nu_cpf', 
        'nu_rg', 'no_orgao_emissor', 'uf_orgao_emissor', 'ds_endereco', 'no_email', 
        'no_email_pessoal', 'nu_telefone', 'dt_alteracaoÍndice', 'url_foto', 'instant_messenger', 
        'icq', 'msn', 'yms', 'ds_comp_end', 'ds_bairro', 
        'nu_cep', 'no_cidade', 'uf_cidade', 'dt_expedicao'
    ];

    /**
     * Permite obtener la informacion asociada en la tabla permissao_sistema
     *  por medio del campo co_usuario
     *
     * @return void
     */
    public function permisoa()
    {
        return $this->hasMany('App\Models\Permisao', 'co_usuario', 'co_usuario');
    }
}
